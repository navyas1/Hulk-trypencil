require('cypress-xpath');
var data=require('../../fixtures/data.json');

describe("Login Page",function(){
    beforeEach(()=>{
        cy.visit('/');
    })
      
    it("Verifying valid Emailid into the Emailid Text Field.",function()
    {
         cy.get('input[name=email]').type(data.email,'{enter}');
        cy.get('p[class="text-destructive text-sm"]').should('not.be.visible');
       

    } )
    it("Verifying invalid Emailid into the Emailid Text Field.",function()
    {
         cy.get('input[name=email]').type(data.invalidemailid,'{enter}');
         cy.contains('Invalid email address').should('be.visible')
        
    } )

    it("Verifying invalid Password into the Password Text Field.",function()
    {
         cy.get('input[name=email]').type(data.email,'{enter}');
         cy.get('input[name=password]').type(data.invalidPassword);
         cy.xpath('//p[@class="text-destructive text-sm"]').should('be.visible');
    } )
    it("Verifying Eye icon",function()
    {
       
         cy.get('input[name=email]').type(data.email,'{enter}');
         cy.get('input[name=password]').type(data.password);
         cy.get('.fill-dark-gray').should('be.visible');
    } )
    it("Verifying Login Button",function()
    {
       
         cy.get('input[name=email]').type(data.email,'{enter}');
         cy.get('input[name=password]').type(data.password);
          cy.xpath('//button[text()="Login"]').click();  
          cy.xpath('//button[text()="New project"]').should('be.visible');
    } )
    it("Verifying invalid Password",function()
    {
         cy.get('input[name=email]').type(data.email,'{enter}');
         cy.get('input[name=password]').type(data.invalidPassword);
          cy.xpath('//button[text()="Login"]').click();  
          cy.xpath('//p[text()="Incorrect credentials"]').should('be.visible');
})

it("Verifying Forget Password Link",function()
{
     cy.xpath('//p[text()="Forget Password"]').click();
     cy.xpath('//div[contains(text(),"Input your email below and we’ll send you a link to reset your password.")]').should('be.visible')
})
it("Forget Password Page: Verifying valid Emailid into the  Emailid Text Field.",function()
{
     cy.xpath('//p[text()="Forget Password"]').click();
     cy.get('input[name=email]').type(data.email,'{enter}');
     cy.xpath('//button[text()="Reset Password"]').click();
     cy.xpath("//div[@class='text-lg font-semibold pb-4']").should('be.visible');
})
it("Forget Password Page: Verifying invalid Emailid into the Emailid Text Field.",function()
{
     cy.xpath('//p[text()="Forget Password"]').click();
     cy.get('input[name=email]').type(data.invalidemailid,'{enter}');
     cy.contains('Invalid email address').should('be.visible')

})
// it("Forget Password Page: Verifying invalid Emailid into the Emailid Text Field.",function()
// {
//     cy.visit('https://hulk.trypencil.com/verify?token=18eae4d06e789c1744e5');
//      cy.get('input[name=password]').type('navyas@12345');
//      cy.get('input[name=passwordConfirm]').type('navyas@12345');
//      cy.xpath('//button[text()="Reset Password"]').click();
//      cy.contains('Invalid email address').should('be.visible')

// })
})